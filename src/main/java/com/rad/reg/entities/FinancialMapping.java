/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rad.reg.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@PersistenceUnit(unitName="RegulatoryReportingPU")
@Entity(name="financials")
@Table(name="financial_mapping_Monthly")
public class FinancialMapping implements Serializable{
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name="id")
  private Long id;
@Column(name="Country")  
  private String country;
@Column(name="LE_Book")
private String leBook;
@Column(name="Year_Month1")
private String yearMonth;
@Column(name="Customer_ID")
private String customerId;
@Column(name="Account_No")
private String accountNumber;
@Column(name="Office_Account")
private String officeAccount;
@Column(name="Vision_GL")
private String visionGl;
@Column(name="Vision_Ouc")
private String visionOuc;
@Column(name="Residents_Flag")
private String residentsFlag;
@Column(name="Currency")
private String currency;
@Column(name="Amount_FCY")
private double amountFcy;
@Column(name="Amount_LCY")
private double amountLcy;
@Column(name="Feed_Date")
@Temporal(TemporalType.TIMESTAMP)
private Date feedDate;
@Column(name="Feed_Status")
private String feedStatus;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the leBook
     */
    public String getLeBook() {
        return leBook;
    }

    /**
     * @param leBook the leBook to set
     */
    public void setLeBook(String leBook) {
        this.leBook = leBook;
    }

    /**
     * @return the yearMonth
     */
    public String getYearMonth() {
        return yearMonth;
    }

    /**
     * @param yearMonth the yearMonth to set
     */
    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    /**
     * @return the customerId
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    /**
     * @return the accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber the accountNumber to set
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return the officeAccount
     */
    public String getOfficeAccount() {
        return officeAccount;
    }

    /**
     * @param officeAccount the officeAccount to set
     */
    public void setOfficeAccount(String officeAccount) {
        this.officeAccount = officeAccount;
    }

    /**
     * @return the visionGl
     */
    public String getVisionGl() {
        return visionGl;
    }

    /**
     * @param visionGl the visionGl to set
     */
    public void setVisionGl(String visionGl) {
        this.visionGl = visionGl;
    }

    /**
     * @return the visionOuc
     */
    public String getVisionOuc() {
        return visionOuc;
    }

    /**
     * @param visionOuc the visionOuc to set
     */
    public void setVisionOuc(String visionOuc) {
        this.visionOuc = visionOuc;
    }

    /**
     * @return the residentsFlag
     */
    public String getResidentsFlag() {
        return residentsFlag;
    }

    /**
     * @param residentsFlag the residentsFlag to set
     */
    public void setResidentsFlag(String residentsFlag) {
        this.residentsFlag = residentsFlag;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the amountFcy
     */
    public double getAmountFcy() {
        return amountFcy;
    }

    /**
     * @param amountFcy the amountFcy to set
     */
    public void setAmountFcy(double amountFcy) {
        this.amountFcy = amountFcy;
    }

    /**
     * @return the amountLcy
     */
    public double getAmountLcy() {
        return amountLcy;
    }

    /**
     * @param amountLcy the amountLcy to set
     */
    public void setAmountLcy(double amountLcy) {
        this.amountLcy = amountLcy;
    }

    /**
     * @return the feedDate
     */
    public Date getFeedDate() {
        return feedDate;
    }

    /**
     * @param feedDate the feedDate to set
     */
    public void setFeedDate(Date feedDate) {
        this.feedDate = feedDate;
    }

    /**
     * @return the feedStatus
     */
    public String getFeedStatus() {
        return feedStatus;
    }

    /**
     * @param feedStatus the feedStatus to set
     */
    public void setFeedStatus(String feedStatus) {
        this.feedStatus = feedStatus;
    }
        

  // ksdfsdafasdfasdfasf
  
}
