/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red.reg.ejbs;

import com.rad.reg.entities.FinancialMapping;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author inzamutuma
 */
@Stateless
public class FinancialMappingEJB {
//@PersistenceUnit(name = "java:app/bnrreports")
//EntityManagerFactory emf1;
@PersistenceContext(unitName="RegulatoryReportingPU")
private EntityManager em;

@PersistenceContext(unitName = "NovanetPU")
private EntityManager emf2;


//@PersistenceContext(unitName = "radiantPU") // as an option
//EntityManager em2;

public List<FinancialMapping> listFinanancialDeclaration()
{
   // EntityManager em = emf1.createEntityManager();
  Query query =  em.createQuery("select t from financials t  where t.yearMonth=:yearmonth ");
  query.setParameter("yearmonth", "201612");
  
  return query.getResultList();
   
}



    
    
}
