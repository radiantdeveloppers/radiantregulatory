/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red.reg.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rad.reg.entities.FinancialMapping;
import com.red.reg.ejbs.FinancialMappingEJB;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author inzamutuma
 */
@Stateless
@Path("regulatory")
public class BnrServices {
    @EJB
    FinancialMappingEJB financialMappingEjb;
    @Path("financialmapping")
    @GET
public String listfinancials() throws Exception
{
    List<FinancialMapping> list = new ArrayList<>();
  list= financialMappingEjb.listFinanancialDeclaration();
    ObjectMapper mapper  = new ObjectMapper();
    System.out.println(mapper.writeValueAsString(list));
    return mapper.writeValueAsString(list);
}
    
}
